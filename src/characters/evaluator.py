# Training Evaluation Tool
#
# By: Luis Leon & Emmanuel Madrigal

import numpy as np

class Char_Evaluator:
    def __init__(self):
        self.cum_loss = 0
        self.cum_predictions = 0
        self.cm = np.zeros((36, 36), dtype=np.int)
        
    def add_sample(self, pred, gtruth, loss):
        # Add predictions
        batch_size = len(gtruth)
        self.cum_predictions += batch_size
        # Accumulate loss
        self.cum_loss += loss
        
        # Confusion matrix superposition
        for i in range(batch_size):
            prediction = pred[i]
            actual = gtruth[i]
            self.cm[actual][prediction] += 1
        
    def compute_metrics(self):
        # Average loss
        loss = self.cum_loss / self.cum_predictions
        # Average precision and recall
        recall = np.diag(self.cm) / np.sum(self.cm, axis = 1)
        precision = np.diag(self.cm) / np.sum(self.cm, axis = 0)
        recall = np.mean(recall)
        precision = np.mean(precision)
        # Average accuracy
        accuracy = np.sum(np.diag(self.cm), axis=0) / self.cum_predictions
        
        return (accuracy, precision, recall, loss)
    
    def reset(self):
        self.cum_loss = 0
        self.cum_predictions = 0
        self.cm = None # Confusion Matrix