# Car Plate Detection and Signaling

This repository contains the implementation and an accompanying paper
for a system in which cars are detected by a camera and its plate
number is extracted through a pattern recognition system.

This project is deliverable for the project of the MP-6122 Pattern
Reconition course in the Electronics Engineering Master program in the
Costa Rica Institute of Technoly.

## Setup Instructions

### Nano-Inference

* Follow the instructions [here](https://developer.nvidia.com/embedded/learn/get-started-jetson-nano-devkit) to setup the NVIDIA Nano.
* Install web server:
```bash
wget https://nodejs.org/dist/v10.16.3/node-v10.16.3-linux-arm64.tar.gz
tar xf node-v10.16.3-linux-arm64.tar.gz
cd node-v10.16.3-linux-arm64/
sudo cp -ra * /usr/local/
```

* Download code
```bash
git clone https://gitlab.com/emmadrigal/mp-6122-car-plate-recognition.git
```

* Install requirements:
** Install pip and opencv
```bash
sudo apt install python3-opencv python3-pip
```
** Install the pytorch wheel
```bash
wget https://nvidia.box.com/shared/static/ncgzus5o23uck9i5oth2n8n06k340l6k.whl -O torch-1.6.0-cp36-cp36m-linux_aarch64.whl
sudo apt-get install python3-pip libopenblas-base libopenmpi-dev 
python3 -m pip install  torch-1.6.0-cp36-cp36m-linux_aarch64.whl
```
** Install torchvision
```bash
sudo apt-get install libjpeg-dev zlib1g-dev
git clone --branch v0.7.0 https://github.com/pytorch/vision torchvision
cd torchvision
sudo python3 setup.py install
cd ../
```

** Install other pip requirements:
```bash
python3 -m pip install cython matplotlib
```
** Install web application dependencies
```bash
cd mp-6122-car-plate-recognition/src/web-receptor/
npm install
cd -
```

* Run application
```bash
cd mp-6122-car-plate-recognition/src
./app.py --image $IMAGE_PATH
```

* Troubleshooting

```bash
ImportError: libcudart.so.10.0: cannot open shared object file: No such file or directory
```

Run the following commands:
```bash
echo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/lib64" >> ~/.bashrc
source ~/.bashrc
```
