/*
    Webserver - Front-end Router
    By: Luis Leon & Emmanuel Madrigal
*/

module.exports = function(app) {
    app.get("/*", function(req, res) {
        // List everything and return using JSON
        res.sendFile(__dirname.substring(0, __dirname.indexOf("back-end")) + "/front-end" + req.url);
    });
}