#!/bin/bash

# Images
wget -c https://s3.eu-central-1.amazonaws.com/avg-kitti/data_object_image_2.zip
# Labels
wget -c https://s3.eu-central-1.amazonaws.com/avg-kitti/data_object_label_2.zip

unzip data_object_image_2.zip
unzip data_object_label_2.zip
