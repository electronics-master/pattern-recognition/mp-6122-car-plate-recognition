#!/usr/bin/env python3

import argparse
import copy
import cv2 as cv

import find_characters as finder
import plate_processing as processor

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Find the characters within a plate')
  parser.add_argument('--image', type=str,
                      help='Location for the plate image', required=True)
  parser.add_argument('--save_sample', type=bool,
                      help='Save a digit', default=False)
  args = parser.parse_args()
  
  image = cv.imread(args.image)
  gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
  
  resized = processor.resize(gray)
  opening = processor.preproc(resized)
  preprocessed = copy.deepcopy(opening)

  output, bbs = finder.bounding_boxes(resized, opening)
  print("Bounding boxes:", bbs)

  if args.save_sample:
    bb = bbs[3]
    cv.imwrite("sample.png", preprocessed[bb[0][1]:bb[1][1], bb[0][0]:bb[1][0]])
  
  cv.imshow("Original", image)
  cv.imshow("Processed", preprocessed)
  cv.imshow("Result", output)
  cv.waitKey(0)
