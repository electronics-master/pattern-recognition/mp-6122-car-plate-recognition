# Add model for the CHARS74 recognition
# Inspired by LeNet 
# http://yann.lecun.com/exdb/publis/pdf/lecun-01a.pdf
# http://www.ee.surrey.ac.uk/CVSSP/demos/chars74k/
#
# By: Luis Leon & Emmanuel Madrigal

import os
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim.lr_scheduler import StepLR
import torch.nn.functional as F
from torchsummary import summary
import wandb

from model import Net
from evaluator import Char_Evaluator

def train_step(model, device, train_loader, optimizer, epoch):
    model.train()
    
    evaluator = Char_Evaluator()
    
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        pred = output.argmax(dim=1, keepdim=True)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        
        # Add sample for evaluation
        evaluator.add_sample(pred.flatten().numpy(), target.numpy(), loss.item())
        
        if batch_idx % 10 == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))
        
    # Print statistics
    acc, prec, rc, loss = evaluator.compute_metrics()
    print("Train acc", acc, "prec", prec, "recall", rc, "loss", loss)
    wandb.log({"Train Accuracy": acc})
    wandb.log({"Train Loss": loss})
    wandb.log({"Train Recall": rc})
    wandb.log({"Train Precision": prec})


def test_step(model, device, test_loader):
    model.eval()
    test_loss = 0
    correct = 0
    
    evaluator = Char_Evaluator()
    
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            loss = F.nll_loss(output, target, reduction='sum')  # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True)  # get the index of the max log-probability
            
            # Add sample for evaluation
            evaluator.add_sample(pred.flatten(), target, loss.item())
            

    # Print statistics
    acc, prec, rc, loss = evaluator.compute_metrics()
    print("Test acc", acc, "prec", prec, "recall", rc, "loss", loss)
    wandb.log({"Test Accuracy": acc})
    wandb.log({"Test Loss": loss})
    wandb.log({"Test Recall": rc})
    wandb.log({"Test Precision": prec})
'''
Main training routine
'''
def train(train_loader, test_loader, device, epochs=100, lr=1e-3):

    model = Net().to(device)
    optimizer = optim.Adam(model.parameters(), lr=lr, eps=1e-4)
    scheduler = StepLR(optimizer, step_size=1, gamma=0.7)
    summary(model,(1,28,28,))
    
    # Log metrics with wandb
    wandb.init(project="emnist-lenet")
    wandb.watch(model)
    
    for epoch in range(epochs):
        train_step(model, device, train_loader, optimizer, epoch)
        test_step(model, device, test_loader)
        scheduler.step()

    # Save model to wandb
    torch.save(model.state_dict(), os.path.join(wandb.run.dir, 'model_char.pt'))
    print('Finished Training')