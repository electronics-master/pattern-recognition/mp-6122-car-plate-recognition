
import os

import numpy as np
from PIL import Image, ImageOps
import torch

import time

import sys

sys.path.append('./plate_detector')

from detector_model import DetectorNet
from draw_boxes import label_to_boxes, draw_box
from preprocess import get_boxes

MODEL_NAME = "detector_model.pt"
INPUT_WIDTH = 640
INPUT_HEIGHT = 320
HORZ_BOXES = 10
VERT_BOXES = 5

BOXES_WIDTH = INPUT_WIDTH // HORZ_BOXES
BOXES_HEIGHT = INPUT_HEIGHT // VERT_BOXES

REFERENCE_BOXES = None
MODEL = None

INPUT_CHANNELS = 3

def run_inference(input_image, device):
    global MODEL, REFERENCE_BOXES

    with torch.no_grad():
        if MODEL is None:
            DetectorNet(INPUT_CHANNELS)
            MODEL = DetectorNet(INPUT_CHANNELS).to(device)
            if os.path.isfile(MODEL_NAME):
                checkpoint = torch.load(MODEL_NAME)
                MODEL.load_state_dict(checkpoint)

    
        orig_shape = input_image.shape
        maxsize = (INPUT_WIDTH, INPUT_HEIGHT)
        pil_image = Image.fromarray(input_image)
        pil_image.thumbnail(maxsize, Image.ANTIALIAS)
        new_shape = pil_image.size


        # Pad with zeros up to expected image width and height
        width_scaling = orig_shape[0] / new_shape[0]
        height_scaling = orig_shape[1] / new_shape[1]

        delta_h = INPUT_HEIGHT - new_shape[1]
        delta_w = INPUT_WIDTH - new_shape[0]

        padding = (delta_w // 2, delta_h // 2, delta_w -
                   (delta_w // 2), delta_h - (delta_h // 2))
        padded_im = np.array(ImageOps.expand(pil_image, padding)).transpose((2, 1, 0))

        torch_image = torch.from_numpy(padded_im).float().to(device)
        output = np.array(
            MODEL(
                torch_image.unsqueeze(0)).cpu())[0].transpose(
                    1, 2, 0)

        if REFERENCE_BOXES == None:
            REFERENCE_BOXES = get_boxes(INPUT_WIDTH, INPUT_HEIGHT,
                                        HORZ_BOXES, VERT_BOXES)

        boxes = label_to_boxes(
            output,
            BOXES_WIDTH,
            BOXES_HEIGHT,
            REFERENCE_BOXES)
                
        padded_im = padded_im.transpose((2, 1, 0))

        # Get crop for all images
        images = []
        for box in boxes:
            x, y, width, height = box

            y -= 5
            x -= 5
            height = int(height * 1.5)
            width = int(width * 1.2)
            # height *= 1.1
            # height = int(height)
            # x = int( x * width_scaling)
            # width = int( width * width_scaling)
            # y = int( y * height_scaling)
            # height = int( height * height_scaling)

            images.append(padded_im[
                 y:y+height,
                 x:x+width,
                 :,
            ])
        
        bbox_image = draw_box(padded_im,
                         boxes,
                         REFERENCE_BOXES,
                         BOXES_WIDTH,
                         BOXES_HEIGHT)

    return images, bbox_image

