/*
    Webserver - TCP script
    By: Luis Leon & Emmanuel Madrigal
*/

var net = require('net');
const kPortTCP = 1337;

module.exports = function(event) {
    /* Create TCP server */
    var server = net.createServer(function(socket) {
        socket.pipe(socket);
        socket.on('data', function(data) {
            event.emit("new_data", data);
        });

        socket.on('error', function(err) {
            console.log("An error occurred with the socket");
        });

        socket.on('close', function(err) {
            console.log("Socket disconnected");
        });
    });

    /* Listen with `nc 127.0.0.1 1337` */
    server.listen(kPortTCP);

}