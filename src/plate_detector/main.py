#!/usr/bin/env python3

from enum import Enum
import argparse
import os
import sys

from torch.utils.data import DataLoader
from torchvision import transforms
import cv2
import matplotlib.pyplot as plt
import numpy as np
import torch
import wandb

from draw_boxes import draw_box, label_to_boxes
from kitti_dataset import KittiCarDataset
from model import Net
from pku_dataset import PKUDataset
from preprocess import get_boxes, get_boxes
from train import train
from transform import ToTensor, Normalize, HorizontalFlip, AddGaussianNoise, ColorJitter, RandomCrop

NUM_WORKERS = 2

class Dataset(Enum):
    kitti = 1
    pku = 2

    def __str__(self):
        return self.name

    @staticmethod
    def from_string(s):
        try:
            return Dataset[s]
        except KeyError:
            raise ValueError()


class Mode(Enum):
    inference = 1
    train = 2
    test = 3

    def __str__(self):
        return self.name

    @staticmethod
    def from_string(s):
        try:
            return Mode[s]
        except KeyError:
            raise ValueError()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Trains a detector')
    parser.add_argument(
        '--training_labels',
        type=str,
        help='Location for the training labels')
    parser.add_argument('--training_images', type=str,
                        help='Location for the training images')
    parser.add_argument(
        '--testing_labels',
        type=str,
        help='Location for the testing labels, if unspecified the trainset will be split')
    parser.add_argument(
        '--testing_images',
        type=str,
        help='Location for the testing images, if unspecified the train set will be split')
    parser.add_argument('--input_width', type=str,
                        help='Image width for training', default=640)
    parser.add_argument('--input_height', type=str,
                        help='Image height for training', default=320)
    parser.add_argument('--horz_boxes', type=str,
                        help='Boxes width for training', default=10)
    parser.add_argument('--vert_boxes', type=str,
                        help='Boxes height for training', default=5)
    parser.add_argument('--batch_size', type=str,
                        help='Batch size', default=128)
    parser.add_argument('--mini_batch_size', type=str,
                        help='Mini Batch size', default=4)
    parser.add_argument('--saved_model', type=str, default=None,
                        help='Saved Model')
    parser.add_argument(
        '--dataset',
        type=Dataset.from_string,
        choices=list(Dataset),
        default=Dataset.pku)
    parser.add_argument(
        '--mode',
        type=Mode.from_string,
        choices=list(Mode),
        default=Mode.train)

    args = parser.parse_args()

    boxes_width = args.input_width // args.horz_boxes
    boxes_height = args.input_height // args.vert_boxes
    reference_boxes = get_boxes(
        args.input_width,
        args.input_height,
        args.horz_boxes,
        args.vert_boxes)

    # setting device on GPU if available, else CPU
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print('Using device:', device)

    if args.mode == Mode.train:
        # Manually seed values for reproducibility, mainly for train/test split
        torch.manual_seed(0)

        torch.multiprocessing.set_start_method('spawn')

        if args.dataset == Dataset.kitti:
            train_dataset = KittiCarDataset(
                args.training_images,
                args.training_labels,
                args.input_width,
                args.input_height,
                args.horz_boxes,
                args.vert_boxes,
                transform=transforms.Compose(
                    [
                        RandomCrop(
                            boxes_width,
                            boxes_height,
                            reference_boxes,
                            args.horz_boxes,
                            args.vert_boxes,
                            top_crop=0.1,
                            bottom_crop=0.1,
                            left_crop=0.1,
                            right_crop=0.1,
                        ),
                        HorizontalFlip(
                            boxes_width,
                            boxes_height,
                            reference_boxes,
                            args.horz_boxes,
                            args.vert_boxes,
                            0.5),
                        ToTensor(device),
                        Normalize(), 
                        ColorJitter(
                            device,
                            brightness=0.25,
                            contrast=0.1,
                            hue=0.25,
                            saturation=0.25,
                        ),
                        AddGaussianNoise(device, std=0.05),
                    ]))
        elif args.dataset == Dataset.pku:
            train_dataset = PKUDataset(
                args.training_images,
                args.training_labels,
                args.input_width,
                args.input_height,
                args.horz_boxes,
                args.vert_boxes,
                transform=transforms.Compose(
                    [
                        RandomCrop(
                            boxes_width,
                            boxes_height,
                            reference_boxes,
                            args.horz_boxes,
                            args.vert_boxes,
                            top_crop=0.1,
                            bottom_crop=0.1,
                            left_crop=0.1,
                            right_crop=0.1,
                        ),
                        HorizontalFlip(
                            boxes_width,
                            boxes_height,
                            reference_boxes,
                            args.horz_boxes,
                            args.vert_boxes,
                            0.5),
                        ToTensor(device),
                        Normalize(),
                        ColorJitter(
                            device,
                            brightness=0.25,
                            contrast=0.1,
                            hue=0.25,
                            saturation=0.25,
                        ),
                        AddGaussianNoise(device),
                    ]))
        else:
            print("Dataset Not Supported")
            sys.exit(1)

        if args.testing_labels and args.testing_images:
            test_dataset = KittiCarDataset(args.testing_images,
                                           args.testing_labels,
                                           args.input_width,
                                           args.input_height,
                                           args.horz_boxes,
                                           args.vert_boxes,
                                           transform=transforms.Compose([
                                               ToTensor(device),
                                               Normalize()
                                           ]))

            train_dataloader = DataLoader(
                train_dataset,
                batch_size=args.mini_batch_size,
                shuffle=True)
            val_dataloader = DataLoader(
                test_dataset,
                batch_size=args.mini_batch_size)

        else:
            train_size = int(len(train_dataset) * 0.8)
            train_set, val_set = torch.utils.data.random_split(
                train_dataset, [train_size, len(train_dataset) - train_size])

            train_dataloader = DataLoader(
                train_set,
                batch_size=args.mini_batch_size,
                shuffle=True,
                num_workers=NUM_WORKERS)
            val_dataloader = DataLoader(
                val_set, batch_size=args.mini_batch_size, num_workers=NUM_WORKERS)

        wandb.init(project="lp-detector")

        train(
            train_dataloader,
            val_dataloader,
            args.batch_size,
            args.mini_batch_size,
            device,
            boxes_width,
            boxes_height,
            reference_boxes,
            args.saved_model)
    elif args.mode == Mode.test:
        if args.dataset == Dataset.kitti:
            dataset = KittiCarDataset(args.testing_images,
                                      args.testing_labels,
                                      args.input_width,
                                      args.input_height,
                                      args.horz_boxes,
                                      args.vert_boxes,
                                      transform=transforms.Compose([
                                          ToTensor(device),
                                          Normalize()
                                      ]))
        elif args.dataset == Dataset.pku:
            dataset = PKUDataset(args.testing_images,
                                 args.testing_labels,
                                 args.input_width,
                                 args.input_height,
                                 args.horz_boxes,
                                 args.vert_boxes,
                                 transform=transforms.Compose([
                                     ToTensor(device),
                                     Normalize(),
                                 ]))
        else:
            print("Dataset not supported")
            sys.exit(1)

        if args.saved_model is None:
            print("A model is required to run the test")
            sys.exit(1)

        model = Net(3).to(device)
        if os.path.isfile(args.saved_model):
            checkpoint = torch.load(args.saved_model)
            model.load_state_dict(checkpoint)
        else:
            print("Path doesn't exist")
            sys.exit(1)

        dataloader = DataLoader(
            dataset,
            batch_size=1,
            num_workers=1)

        with torch.no_grad():
            # This code is used to test that a dataset labels are correct
            for k in range(len(dataset)):
                sample = dataset[k]

                image = np.array(sample["image"].cpu()).transpose(2, 1, 0)
                # output = np.array(sample["box"]).transpose(1, 2, 0)
                output = np.array(
                    model(
                        sample["image"].unsqueeze(0)).cpu())[0].transpose(
                    1, 2, 0)

                boxes = label_to_boxes(
                    output, boxes_width, boxes_height, reference_boxes)
                image = draw_box(image,
                                 boxes,
                                 reference_boxes,
                                 boxes_width,
                                 boxes_height)

                plt.imshow(image)
                plt.show()

    elif args.mode == Mode.inference:
        print("Non-implemented mode, this will capture from a camera or video")
        sys.exit(1)
