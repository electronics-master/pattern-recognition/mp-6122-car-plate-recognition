# Extract the bounding boxes according to the constraints
# By: Luis Leon & Emmanuel Madrigal

import cv2 as cv
import numpy as np
from scipy.ndimage.measurements import label
import copy

def draw_labeled_bboxes(img, labels, padding=0, min_size=(5, 40), max_size=(64, 86)):
    """
    Draw the boxes on the detected characters
    """
    bb_list = list([])
    for i in range(1, labels[1]+1):
        # Find pixels with each car label value
        nonzero = (labels[0] == i).nonzero()
        # Identify x and y values of those pixels
        nonzeroy = np.array(nonzero[0])
        nonzerox = np.array(nonzero[1])
        # Define a bounding box based on min/max x and y
        xdim = (np.max(nonzerox) - np.min(nonzerox))
        ydim = (np.max(nonzeroy) - np.min(nonzeroy))
        if xdim >= min_size[0] and ydim >= min_size[1] and \
            xdim <= max_size[0] and ydim <= max_size[1]:
                
            bbox = ((np.min(nonzerox)-padding, np.min(nonzeroy)-padding), (np.max(nonzerox)+padding, np.max(nonzeroy)+padding))
            cv.rectangle(img, bbox[0], bbox[1], 255, 2)
            bb_list.append((bbox[0], bbox[1]))
    return img, bb_list

def label_boxes(original, markers):
    heat = np.zeros_like(original[:,:]).astype(np.float) 
    heat[markers == 1] = 255

    labels = label(heat)
    labeled = copy.deepcopy(original)
    _ , bb_list = draw_labeled_bboxes(labeled, labels)
    return labeled, bb_list

def sorter(bb):
    return bb[0][0]

def bounding_boxes(original, negative):
    negative[negative == 255] = 1
    img, bbs = label_boxes(original, negative)
    bbs.sort(key=sorter)
    return img, bbs
