import copy
import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
import sys

# Import project modules
import characters.inference as Inference
import characters.find_characters as Finder
import characters.plate_processing as PlateProcessor
import characters.char_processing as CharProcessor

'''
General usage:

1. Find the Characters
2. Inference

Example:

bbs, preprocessed_image = find_characters(licence_plate_img)
plate_number = get_plate_number(bbs, preprocessed_image)
'''

def find_characters(licence_plate_img, invert=True):
    '''
    This stage extracts the character bounding boxes from the image. This 
    applies some preprocessing in order to achieve the extraction

    Parameters:
    - Cropped licence plate image
    - Invert: inverts the image (black digits against white background)
    Return:
    - bounding boxes
    - preprocessed image
    '''
    print("Performing preprocessing on the licence plate")
    gray = cv.cvtColor(licence_plate_img, cv.COLOR_BGR2GRAY)
    resized = PlateProcessor.resize(gray)
    opening = PlateProcessor.preproc(resized, invert)
    preprocessed = copy.deepcopy(opening)

    print("Finding bounding boxes")
    _, bbs = Finder.bounding_boxes(resized, opening)
    return bbs, preprocessed

def get_plate_number(character_bbs, preprocessed, device):
    '''
    This stage extracts the character bounding boxes from the image. This 
    applies some preprocessing in order to achieve the extraction

    Parameters:
    - bounding boxes
    - preprocessed image

    Return:
    - string with the prediction
    '''
    chars = []
    print("Inferring characters")
    for bb in character_bbs:
        cropped = preprocessed[bb[0][1]:bb[1][1], bb[0][0]:bb[1][0]]
        prec = CharProcessor.preprocess_digit(cropped)
        character = Inference.predict_char(prec, device, model_location="characters/model_char.pt")
        chars.append(character)
    return "".join(chars)
