# Inference by using the CHARS74 recognition
# Inspired by LeNet 
# http://yann.lecun.com/exdb/publis/pdf/lecun-01a.pdf
#
# By: Luis Leon & Emmanuel Madrigal

import os
import torch
from torch.autograd import Variable
import sys

sys.path.append('./characters')

from model import Net
from chars74_dataset import get_char

MODEL = None

def load_model(model_location):
    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")
    model = Net().to(device)
    model.load_state_dict(torch.load(model_location))
    model.eval()
    return model

def predict_char(img, device, model_location="model_char.pt", confidence=0.7):
    global idx, MODEL
    if MODEL is None:
        MODEL = load_model(model_location)
    test_output = MODEL(Variable(torch.FloatTensor(img).unsqueeze(0).unsqueeze(0)).to(device))

    pred = test_output.argmax(dim=1, keepdim=True)
    return get_char(pred[0])
