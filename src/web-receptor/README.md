# Web framework

## Installation

Install NodeJS:

```bash
wget https://nodejs.org/dist/v10.16.3/node-v10.16.3-linux-x64.tar.xz
tar xf node-v10.16.3-linux-x64.tar.xz
cd node-v10.16.3-linux-x64
sudo cp -ra * /usr/local/
```

Install dependencies:

```bash
npm install
```

Run:

```bash
npm start
```

The server will listen on: http://localhost:3333/