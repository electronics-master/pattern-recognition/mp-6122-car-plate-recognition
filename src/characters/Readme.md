# Licence plate reader

Follow these steps to make it work

## Download dataset

```bash
./download_char74_dataset.sh
```

## Identify the characters (optional for verifying that the input is extracted correctly)

```bash
./extract_characters.py --image $IMAGE
```

## Run the training and operations related to the NN

```bash
./main.py --batch_size $BS --epochs $EP --lr $LR
```

## Run the inference over a plate

```bash
./decypher_characters.py --image $IMAGE
```
