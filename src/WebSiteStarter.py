# Website client starter
# By: Luis Leon & Emmanuel Madrigal

import socket
import subprocess
import time

'''
General usage:

1. Launch the server
3. Send data over the socket
4. Finalize the server

Example:
web = WebService()
web.send_data("DATA")
del web
'''

class WebService():
    def __init__(self, location="web-receptor", host='127.0.0.1', port=1337):
        self.process = None
        self.socket = None

        # Initialise node
        self.process = subprocess.Popen(['node', location+'/server.js'], \
            stdout=subprocess.PIPE)
        time.sleep(3)
        # Get socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = (host, port)
        sock.connect(server_address)
        self.socket = sock
    
    def send_data(self, data):
        self.socket.sendall(data.encode())

    def __del__(self):
        self.process.terminate()
