#!/usr/bin/env python3

# Characters recognition
# Inspired by LeNet 
# http://yann.lecun.com/exdb/publis/pdf/lecun-01a.pdf
# http://www.ee.surrey.ac.uk/CVSSP/demos/chars74k/
#
# By: Luis Leon & Emmanuel Madrigal

import argparse

import cv2
import torch
from torchvision import transforms
from torch.utils.data import DataLoader
import wandb

from train import train
from chars74_dataset import CHARS74

if __name__ == "__main__":
    # Seed for controlling the pseudo-random generator
    torch.manual_seed(127)

    # setting device on GPU if available, else CPU
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print('Using device:', device)
    print()

    torch.multiprocessing.set_start_method('spawn')

    parser = argparse.ArgumentParser(description='Trains a CHARS74 Classifier')
    parser.add_argument('--batch_size', type=int,
                        help='Batch size', default=100)
    parser.add_argument('--epochs', type=int,
                        help='Epochs', default=10)
    parser.add_argument('--lr', type=float,
                        help='Learning rate', default=1e-3)                  

    args = parser.parse_args()

    # Load the dataset
    train_loader = torch.utils.data.DataLoader(
       CHARS74('data/CHARS74', train=True,
                    transform=transforms.Compose([
                        transforms.ToTensor(),
                        transforms.Normalize((0.1307,), (0.3081,))
                    ])),
        batch_size=args.batch_size, shuffle=True)

    test_loader = torch.utils.data.DataLoader(
        CHARS74('data/CHARS74', train=False, 
                    transform=transforms.Compose([
                        transforms.ToTensor(),
                        transforms.Normalize((0.1307,), (0.3081,))
                    ])),
        batch_size=int(args.batch_size/2), shuffle=True, num_workers=4)

    # Perform training
    train(train_loader, test_loader, device, args.epochs, args.lr)
