#!/usr/bin/env python3

# Trace receiver
# By: Luis Leon & Emmanuel Madrigal

import json
import random
import socket
import random
import time

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 1337        # The port used by the server

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = (HOST, PORT)
sock.connect(server_address)

while 1:
  time.sleep(1)
  rand_int = str(int(random.random() * 1000000))
  sock.sendall(rand_int.encode())
  print("Sent:", rand_int)