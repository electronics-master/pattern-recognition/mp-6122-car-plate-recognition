import numpy as np
import torch


def update_map(
        detection_map,
        training_labels,
        network_outputs,
        batch_size,
        box_width,
        box_height,
        reference_boxes):

    pred_bb = []
    pred_classes = []
    pred_conf = []
    gt_bb = []
    gt_classes = []

    for network_output, truth_label in zip(network_outputs, training_labels):
        for box in network_output.detach().cpu().numpy().transpose(0, 2, 3, 1):
            for i in range(box.shape[0]):
                for j in range(box.shape[1]):
                    conf = box[i][j][0]
                    x = box[i][j][1] * box_width + reference_boxes[i][j][0]
                    y = box[i][j][2] * box_height + reference_boxes[i][j][1]
                    width = box[i][j][3] * box_width
                    height = box[i][j][4] * box_height

                    pred_bb.append(np.array([x, y, x + width, y + height]))
                    pred_classes.append(np.array(0))
                    pred_conf.append(np.array(conf))

    for network_output, truth_label in zip(network_outputs, training_labels):
        for box in truth_label.detach().cpu().numpy().transpose(0, 2, 3, 1):
            for i in range(box.shape[0]):
                for j in range(box.shape[1]):
                    if (box[i][j][0] > 0):
                        x = box[i][j][1] * box_width + reference_boxes[i][j][0]
                        y = box[i][j][2] * box_height + \
                            reference_boxes[i][j][1]
                        width = box[i][j][3] * box_width
                        height = box[i][j][4] * box_height

                        gt_bb.append(np.array([x, y, x + width, y + height]))
                        gt_classes.append(np.array(0))

    pred_bb = np.array(pred_bb)
    pred_classes = np.array(pred_classes)
    pred_conf = np.array(pred_conf)
    gt_bb = np.array(gt_bb)
    gt_classes = np.array(gt_classes)

    detection_map.evaluate(pred_bb, pred_classes, pred_conf, gt_bb, gt_classes)


def get_metrics(detection_map):

    precision, recall = detection_map.compute_precision_recall_(0)

    ap = detection_map.compute_ap(precision, recall)
    precision = np.average(precision)
    recall = np.average(recall)

    return ap, precision, recall
