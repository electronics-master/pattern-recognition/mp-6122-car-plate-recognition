import cv2
import kornia
import numpy as np
import sys
import torch
import torchvision

from draw_boxes import label_to_boxes
from preprocess import boxes_to_output


class HorizontalFlip(object):
    def __init__(
            self,
            boxes_width,
            boxes_height,
            reference_boxes,
            boxes_x,
            boxes_y,
            flip_chance=0.):
        self.flip_chance = flip_chance
        self.boxes_width = boxes_width
        self.boxes_height = boxes_height
        self.reference_boxes = reference_boxes
        self.boxes_x = boxes_x
        self.boxes_y = boxes_y

    def __call__(self, sample):
        image, label = sample['image'], sample['box']

        image_height, image_width, _ = image.shape

        if self.flip_chance < np.random.uniform():
            flipped_image = np.fliplr(image).copy()

            boxes = label_to_boxes(
                label,
                self.boxes_width,
                self.boxes_height,
                self.reference_boxes)

            new_boxes = []
            for box in boxes:
                x, y, width, height = box

                x = image_width - x - width

                new_boxes.append((x, y, width, height))
            label = boxes_to_output(
                self.reference_boxes,
                new_boxes,
                image_width,
                image_height,
                self.boxes_x,
                self.boxes_y)

        else:
            flipped_image = image

        return {'image': flipped_image,
                'box': label}


class RandomCrop(object):
    def __init__(self,
                 boxes_width,
                 boxes_height,
                 reference_boxes,
                 boxes_x,
                 boxes_y,
                 top_crop=0., bottom_crop=0., left_crop=0., right_crop=0.):
        self.top_crop = top_crop
        self.bottom_crop = bottom_crop
        self.left_crop = left_crop
        self.right_crop = right_crop
        self.reference_boxes = reference_boxes
        self.boxes_width = boxes_width
        self.boxes_height = boxes_height
        self.boxes_x = boxes_x
        self.boxes_y = boxes_y

    def __call__(self, sample):
        image, label = sample['image'], sample['box']

        image_height, image_width, _ = image.shape

        top = int(np.random.uniform(0, self.top_crop) * image_height)
        bottom = int(
            image_height -
            np.random.uniform(
                0,
                self.bottom_crop) *
            image_height)
        left = int(np.random.uniform(0, self.left_crop) * image_width)
        right = int(
            image_width -
            np.random.uniform(
                0,
                self.right_crop) *
            image_width)

        cropped_image = image[
            top:bottom,
            left:right,
            :
        ]

        new_height, new_width, _ = cropped_image.shape

        width_scaling = image_width / new_width
        height_scaling = image_height / new_height

        cropped_image = cv2.resize(cropped_image, (image_width, image_height))

        boxes = label_to_boxes(
            label,
            self.boxes_width,
            self.boxes_height,
            self.reference_boxes)

        new_boxes = []
        for box in boxes:
            x, y, width, height = box

            x -= left
            y -= top

            x *= width_scaling
            width *= width_scaling

            y *= height_scaling
            height *= height_scaling

            new_boxes.append((x, y, width, height))

        new_label = boxes_to_output(
            self.reference_boxes,
            new_boxes,
            image_width,
            image_height,
            self.boxes_x,
            self.boxes_y)

        return {'image': cropped_image,
                'box': new_label}


class ColorJitter(object):
    def __init__(
            self,
            device,
            brightness=0.,
            saturation=0.,
            hue=0.,
            contrast=0.,
            gamma=0.):
        self.brightness = brightness
        self.saturation = saturation
        self.hue = hue
        self.contrast = contrast
        self.gamma = gamma

        self.device = device

    def __call__(self, sample):
        image, box = sample['image'], sample['box']

        brightness = np.random.uniform(-self.brightness, self.brightness)
        contrast = np.random.uniform(1 - self.contrast, 1 + self.contrast)
        hue = np.random.uniform(1 - self.hue, 1 + self.hue)
        saturation = np.random.uniform(
            1 - self.saturation, 1 + self.saturation)

        image = kornia.adjust_brightness(image, brightness)
        image = kornia.adjust_contrast(image, contrast)
        image = kornia.adjust_hue(image, hue)
        image = kornia.adjust_saturation(image, saturation)

        image = image.to(self.device)

        return {'image': image,
                'box': box}


class AddGaussianNoise(object):
    def __init__(self, device, mean=0., std=0.05):
        self.device = device
        self.std = std
        self.mean = mean

    def __call__(self, sample):
        image = sample['image']

        noisy_image = image + (torch.randn(image.size())
                               * self.std + self.mean).to(self.device)
        
        return {'image': noisy_image,
                'box': sample['box']
                }


class Normalize(object):
    """Normalize images from 0-255 to 0-1"""

    def __call__(self, sample):
        image = sample['image']

        tensor_image_0_1 = image / 255.0

        return {'image': tensor_image_0_1,
                'box': sample['box']}


class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __init__(self, device):
        self.device = device

    def __call__(self, sample):
        image, boxes = sample['image'], sample['box']

        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X W X H
        image = image.transpose((2, 1, 0))
        image = torch.from_numpy(image).float().to(self.device)
        if boxes is not None:
            boxes = boxes.transpose((2, 0, 1))
            boxes = torch.from_numpy(boxes).float().to(self.device)

        return {'image': image,
                'box': boxes}
