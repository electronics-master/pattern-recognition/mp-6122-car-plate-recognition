#!/usr/bin/env python3

# CHARS74 dataset:
# http://www.ee.surrey.ac.uk/CVSSP/demos/chars74k/
#
# By: Luis Leon & Emmanuel Madrigal

import cv2
import torch
from torch.utils.data import Dataset
from torchvision import transforms
import glob
import os
import numpy as np
import cv2 as cv
from PIL import Image

import matplotlib.pyplot as plt

def default_img_transform(img):
    rimg = cv.resize(img, (28,28))
    _, otsu = cv.threshold(rimg, 0, 255, cv.THRESH_OTSU + cv.THRESH_BINARY_INV)
    return otsu

def get_label(filename):
    split = filename.split('/')
    img_prefix = split[-1].split('-')
    cat = img_prefix[0].split('img')
    return int(cat[1]) - 1

def data_transform(obj):
    img = obj["image"]
    label = obj["label"]
    
def get_char(label):
    idx = ['0','1','2','3','4','5','6','7','8','9',
       'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
    return idx[label]

class CHARS74(Dataset):
    def __init__(self, root_dir, train=True, transform=None):
        '''
        root_dir goes to the path CHARS74
        transform defaults to None
        '''
        self.labels = []
        self.image_filenames = []

        # Load files
        for image_path in os.walk(root_dir):
            files = glob.glob(image_path[0] + "/*png")
            total = len(files)
            offset = 0
            last_idx = int(0.75 * total)
            if not train:
                offset = last_idx
                last_idx = total
            
            for i in range(offset, last_idx):
                filename = files[i]
                # Append the path
                self.image_filenames.append(filename)
                self.labels.append(get_label(filename))


        self.transform = transform

        if not train:
            print("Testing set", len(self.labels))
        else:
            print("Training set", len(self.labels))

    def __len__(self):
        return len(self.image_filenames)

    def __getitem__(self, idx):
        # Load image
        img = cv.imread(self.image_filenames[idx], 0)
        img_cam_ready = default_img_transform(img)

        # Load label
        label = self.labels[idx]
        
        if self.transform:
            img_cam_ready = self.transform(img_cam_ready)

        return (img_cam_ready, label)

if __name__ == "__main__":

    train_loader = torch.utils.data.DataLoader(
        CHARS74('data/CHARS74',
                    transform=transforms.Compose([
                        transforms.ToTensor(),
                        transforms.Normalize((0.1307,), (0.3081,))
                    ])), batch_size=int(10), shuffle=True)

    for batch_idx, (data, target) in enumerate(train_loader):
        print(get_char(target[0].numpy()))
        plt.imshow(np.squeeze(data[0]),cmap='bone')
        plt.show()
